<?php

namespace Drupal\ranking_field\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ranking_field\Utility\RankingUtility;

use Drupal\node\NodeStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Created by PhpStorm.
 * User: shimon
 * Date: 06/05/18
 * Time: 10:51
 */
class RankingController extends ControllerBase
{

  /**
   * The storage handler class for nodes.
   *
   * @var NodeStorage
   */


  private $nodeStorage;

  /**
   * Class constructor.
   *
   * @param EntityTypeManagerInterface $entity
   *   The Entity type manager service.
   * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
   */

  public function __construct(EntityTypeManagerInterface $entity)
  {
    $this->nodeStorage = $entity->getStorage('node');
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function updateRank()
  {

    $table = \Drupal::request()->query->get('table');
    $stars = \Drupal::request()->query->get('stars');
    $nid = \Drupal::request()->query->get('nid');


    $node = $this->nodeStorage->load($nid);

    if ($this->validate($table, $stars, $node))
      $this->addRateByOrm($table, $stars, $node);

    die;

  }

  /**
   * @param $table
   * @param $stars
   * @param $node
   */
  private function addRateByOrm($table, $stars, $node)
  {


    $raters = $node->get($table)->getValue()[0]['number_of_raters'];
    $oldStars = $node->get($table)->getValue()[0]['number_of_stars'];

    
    $node->set($table, [
      'number_of_raters' => $raters + 1,
      'number_of_stars' => $oldStars + $stars,
    ]);
    $node->save();
  }

  private function validate($table, $stars, $node)
  {
    return ($stars > 0 && $stars <= 5) && (!is_null($node)) && ($node->hasField($table));
  }

}
