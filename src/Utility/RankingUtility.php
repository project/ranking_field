<?php


namespace Drupal\ranking_field\Utility;


class RankingUtility {


  public static function getFieldValue($node, $field) {

    if ($node && $node->hasField($field) && !$node
        ->get($field)
        ->isEmpty()) {
      return $node->get($field)->getString();
    }
    return NULL;
  }
}
