CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The RankingField module adds a Ranking Field to drupal fields, attractive
voting/ranking to nodes and any entity that you can add a drupal native fields.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ranking_field

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/ranking_field

REQUIREMENTS
------------
 * This module requires no modules outside of the Drupal core.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. For further
information, see:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

CONFIGURATION
-------------
 * After enable, this module adds a Ranking Field to drupal fields, attractive
   voting/ranking to nodes and any entity.

MAINTAINERS
-----------
Current maintainer:
  * amarshimi - https://www.drupal.org/u/amarshimi
